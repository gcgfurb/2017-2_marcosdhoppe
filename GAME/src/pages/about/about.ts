import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Game } from '../../models/game.model';

export interface Item {
  image: string;
  imgCorrect: boolean;
  clicked: boolean;
}

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  jogo: Game = {} as Game;
  continuaJogo: boolean = true;
  itens: Item[] = [];


  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    this.iniciaAtributoJogo();
    this.inicializaVetorImg();
  }


  verificaImagens() {
    for (var i = 0; i < this.itens.length; i++) {
      if (this.itens[i].imgCorrect === this.itens[i].clicked) {
        this.continuaJogo = false;
      } else {
        this.continuaJogo = true;
        break;
      }
    }

    if (!this.continuaJogo) {
      this.presentAlert("Parabéns!!! você acertou todas as imagens.");
    } else {
      this.presentAlert("Ops... imagens selecionas incorretas ou faltando! Tente novamente.");
    }
  }


  presentAlert(mensagem: string) {
    const alert = this.alertCtrl.create({
      message: mensagem,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }

  shuffle(array: any[]) {
    return array.sort(() => { return 0.5 - Math.random() });
  }

  inicializaVetorImg() {
    for (var i = 0; i < this.jogo.imagensCertas.length; i++) {
      this.itens.push({ image: this.jogo.imagensCertas[i], imgCorrect: true, clicked: false })
    }
    for (var j = 0; j < this.jogo.imagensErradas.length; j++) {
      this.itens.push({ image: this.jogo.imagensErradas[j], imgCorrect: false, clicked: false })
    }

    this.shuffle(this.itens);
    this.shuffle(this.itens);
  }

  iniciaAtributoJogo() {
    this.jogo.name = "jogo das imagens";
    this.jogo.imagensCertas = ["assets/img/user1.png", "assets/img/user1.png", "assets/img/user1.png", "assets/img/user1.png"];
    this.jogo.imagensErradas = ["assets/img/user2.png", "assets/img/user3.png", "assets/img/user2.png", "assets/img/user4.png"];
    this.jogo.tempo = "00:00:00";
    this.jogo.pergunta = "Quais animais abaixo são ursos?";
  }

}
