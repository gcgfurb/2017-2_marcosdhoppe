import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


export interface Item {
  image: string;
  imgCorrect: boolean;
  clicked: boolean;
}

@IonicPage()
@Component({
  selector: 'page-teste',
  templateUrl: 'teste.html',
})
export class TestePage {


  itens: Item[] = [
    { image: "assets/img/user1.png", imgCorrect: true, clicked: false },
    { image: "assets/img/user2.png", imgCorrect: false, clicked: false },
    { image: "assets/img/user1.png", imgCorrect: true, clicked: false },
    { image: "assets/img/user2.png", imgCorrect: false, clicked: false }
    
  ];


  showStyle = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  verificaImagens() {
    for (var i = 0; i < this.itens.length; i++) {
      if (this.itens[i].imgCorrect === this.itens[i].clicked) {
      } else {
        console.log("perdeu o jogo tente novamente!");
        break;

      }
    }
  }

  clicando(index: number) {
    this.itens[index].clicked = !this.itens[index].clicked;
  }

}
