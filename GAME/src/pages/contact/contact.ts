import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Game } from '../../models/game.model';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  jogo: Game = {} as Game;

  cards: string[] = [];
  duasCartas: number[] = [];
  imagens: string[] = [];

  constructor(public navCtrl: NavController) {
    this.iniciaAtributoJogo();
    this.cards = new Array(this.jogo.imagens.length * 2);
    this.cards = this.jogo.imagens.concat(this.jogo.imagens);
    this.shuffle(this.cards);

  }


  shuffle(array: any[]) {
    return array.sort(() => { return 0.5 - Math.random() });
  }


  cardClick(index) {
    this.duasCartas.push(index);
    if (this.duasCartas.length == 2) {

      if (this.cards[this.duasCartas[0]] == this.cards[this.duasCartas[1]]) {
        console.log("sim as cartas sao iguais");
      } else {
        console.log("não as cartas sao diferentes");
      }

      this.duasCartas = [];
    }
  }

  iniciaAtributoJogo() {
    this.jogo.name = "jogo da memoria";
    this.jogo.imagens = ["assets/img/user1.png", "assets/img/user2.png", "assets/img/user3.png", "assets/img/user4.png"];
    this.jogo.tempo = "00:10:00";
  }


}
