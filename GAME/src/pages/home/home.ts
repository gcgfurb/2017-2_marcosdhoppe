import { Component } from '@angular/core';
import { FabContainer, NavController, AlertController, Keyboard } from 'ionic-angular';
import { Game } from '../../models/game.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  jogo: Game = {} as Game;

  dica: string = "";
  qtdErro: number = 0;
  letra: string = "";
  letraDigitadas = "";
  palavra: string;
  vetorLetras: string[] = [];

  constructor(
    public alertCtrl: AlertController,
    public keyboard: Keyboard,
    public navCtrl: NavController
  ) {
   // console.log("teclado aberto: ", keyboard.isOpen());
    
    this.iniciaAtributoJogo();                                          
    this.iniciaVetorVazio();
  }


  iniciaVetorVazio() {
    for (var i = 0; i < this.jogo.resposta.length; i++) {
      if (this.jogo.resposta[i].indexOf(" ")) {
        this.vetorLetras.push("_");
      } else {
        this.vetorLetras.push("-");
      }
    }
    this.palavra = this.jogo.resposta.toUpperCase();
  }


  verificaLetra() {
    if (this.letra[0] != "") {
      this.letra = this.letra[0].toUpperCase();

      if (this.jogo.resposta.toUpperCase().indexOf(this.letra) == -1) {                  // verifica se existe a letra de entrada na palavra
        this.qtdErro++;

        if (this.qtdErro == 1) {                                                         // tratamento do , das letras
          this.letraDigitadas = this.letra;
        } else {
          this.letraDigitadas = this.letraDigitadas + ", " + this.letra;
        }
        this.letra = "";

        if ("" + this.qtdErro == this.jogo.dificuldade) {                                 // verifica fim do jogo pela quantidade de erros
          this.presentAlert("Game Over", "Tente novamente!");
        }

      } else {
        for (var i = 0; i < this.jogo.resposta.length; i++) {                             // verifica todas as letras da palavra em maiuscula 
          if (this.jogo.resposta.toUpperCase()[i] == this.letra) {
            this.vetorLetras[i] = this.jogo.resposta[i];

            if (this.vetorLetras.indexOf("_") == -1) {                                    // verifica se ja encontrou todas as letras
              this.presentAlert("Parabéns!!!", "Você ganhou o jogo");
            }
          }

        }
        this.letra = "";
      }
    }

  }


  abrirDica(tipoDica: string, fab: FabContainer) {
    this.dica = tipoDica;
    fab.close();
  }

  presentAlert(titulo: string, mensagem: string) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensagem,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }

  iniciaAtributoJogo() {
    this.jogo.name = "jogo 1";
    this.jogo.dificuldade = "4";
    this.jogo.resposta = "Urso Pardo";
    this.jogo.photo = "assets/img/user1.png";
    this.jogo.pergunta = "Animal muito perigoso?";
    this.jogo.tempo = "00:10:00";
  }


}
