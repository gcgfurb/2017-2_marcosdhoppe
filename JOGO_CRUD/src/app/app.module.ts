import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { GameProvider } from '../providers/game/game';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';

export const FIREBASE_APP_CONFIG: FirebaseAppConfig = {
  apiKey: "AIzaSyDNXmK8M3nIKBVF0fsGFIlUX7zKsv3YK-w",
  authDomain: "teste-b4c27.firebaseapp.com",
  databaseURL: "https://teste-b4c27.firebaseio.com",
  projectId: "teste-b4c27",
  storageBucket: "teste-b4c27.appspot.com",
  messagingSenderId: "94383418047"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(FIREBASE_APP_CONFIG),
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    GameProvider,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
