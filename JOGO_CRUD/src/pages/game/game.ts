import { FirebaseListObservable } from 'angularfire2/database';
import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { GameProvider } from '../../providers/game/game';
import { Game } from '../../models/game.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {

  formGame: FormGroup;
  games: FirebaseListObservable<Game[]>;

  constructor(
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public gameProvider: GameProvider,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {

    this.formGame = formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]],
    });

    this.games = gameProvider.getAll();
  }

  newGame() {
    this.gameProvider.create(this.formGame.value)
      .then(() => {
        this.formGame.reset();
        console.log("Game cadastrado com sucesso");
      });
  }

  excluir(game: Game) {
    this.gameProvider.delete(game.$key)
      .then(() => {
        console.log("Game excluido com sucesso");
      });
  }


  editar(game: Game) {
    const alert = this.alertCtrl.create({
      title: 'Edição',
      inputs: [
        {
          name: 'name',
          value: game.name,
          type: 'text'
        },
        {
          name: 'description',
          value: game.description,
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
            this.gameProvider.update(game.$key, data)
              .then(() => {
                console.log("Game editado com sucesso");
              });
          }
        }
      ]
    });
    alert.present();
  }



}
