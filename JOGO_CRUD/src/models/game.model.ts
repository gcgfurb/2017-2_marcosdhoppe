export interface Game {
    $key?: string;
    name: string;
    description: string;
}