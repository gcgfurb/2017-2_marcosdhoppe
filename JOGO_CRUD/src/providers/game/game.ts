import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { Game } from '../../models/game.model';
import * as firebase from 'firebase/app';

@Injectable()
export class GameProvider {

  jogosListRef: FirebaseListObservable<Game[]>;

  constructor(
    private afDb: AngularFireDatabase
  ) {

    this.jogosListRef = this.afDb.list('/games', {
      query: {
        orderByChild: 'name'
      }
    });
  }

  getAll(): FirebaseListObservable<Game[]> {
    return this.jogosListRef;
  }

  create(gameObj: Game): firebase.database.ThenableReference {
    return this.jogosListRef.push(gameObj);
  }

  delete(gameKey: string): firebase.Promise<void> {
    return this.jogosListRef.remove(gameKey);
  }

  update(gameKey: string, newObjGame: any): firebase.Promise<void> {
    return this.jogosListRef.set(gameKey, newObjGame);
  }

}

