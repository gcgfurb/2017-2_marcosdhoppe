import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { PageInterface } from '../models/page.model';
import { User } from '../models/user.model';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;
  
  @ViewChild(Nav) nav: Nav;

  usuario: User = {} as User;

  pages: PageInterface[] = [
    { title: 'Editor', pageName: 'TabsPage', tabComponent: 'TabEditorPage', index: 0, icon: 'construct' },
    { title: 'Jogos', pageName: 'TabsPage', tabComponent: 'TabGamePage', index: 1, icon: 'game-controller-b' },
    { title: 'Perfil', pageName: 'TabsPage', tabComponent: 'TabProfilePage', index: 2, icon: 'person' },
    { title: 'Sobre', pageName: 'AboutPage', icon: 'information-circle' },
    { title: 'Tutorial', pageName: 'TutorialPage', icon: 'hammer' }
  ];

  constructor(
    afAuth: AngularFireAuth,
    afDb: AngularFireDatabase,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {

    afAuth.authState.subscribe((authState: firebase.User) => {
      if (authState) {
        afDb.object(`/users/${authState.uid}`)
          .subscribe((currentUser: User) => {
            this.usuario = currentUser;
          });
        this.rootPage = 'TabsPage';
      } else {
        this.rootPage = 'TutorialPage';
      }
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  // Define a pagina a ser aberta, se estiver na tabs passa o index
  openPage(page: PageInterface) {
    let params = {};
    if (page.index) {
      params = { tabIndex: page.index };
    }

    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {
      this.nav.setRoot(page.pageName, params)
        .catch((err: any) => {
          console.log(`Didn't set nav root: ${err}`);
        });
    }
  }

  // Realça a cor da pagina ativa (root) no menu e/ou na tabs 
  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNavs()[0];
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary';
    }
  }

}
