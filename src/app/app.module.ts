import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { AlbumCreateComponent } from '../components/album-create/album-create';
import { MyApp } from './app.component';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';

import { Camera } from '@ionic-native/camera';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AlbumProvider } from '../providers/album/album';
import { AuthProvider } from '../providers/auth/auth';
import { CompartilhaProvider } from '../providers/compartilha/compartilha';
import { GameProvider } from '../providers/game/game';
import { UserProvider } from '../providers/user/user';

import { GameImportComponent } from '../components/game-import/game-import';
import { ListUserComponent } from '../components/list-user/list-user';
import { ListAlbumComponent } from '../components/list-album/list-album';

export const FIREBASE_APP_CONFIG: FirebaseAppConfig = {
  apiKey: "AIzaSyARznUH4VJgbh7asu_wXdApLR83yGaMBSg",
  authDomain: "edibox-a777c.firebaseapp.com",
  databaseURL: "https://edibox-a777c.firebaseio.com",
  projectId: "edibox-a777c",
  storageBucket: "edibox-a777c.appspot.com",
  messagingSenderId: "621339284983"
};

@NgModule({
  declarations: [
    AlbumCreateComponent,
    GameImportComponent,
    ListAlbumComponent,
    ListUserComponent,
    MyApp
  ],
  imports: [
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(FIREBASE_APP_CONFIG),
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AlbumCreateComponent,
    GameImportComponent,
    ListAlbumComponent,
    ListUserComponent,
    MyApp
  ],
  providers: [
    AngularFireDatabase,
    AlbumProvider,
    AuthProvider,
    Camera,
    CompartilhaProvider,
    GameProvider,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserProvider
  ]
})
export class AppModule { }
