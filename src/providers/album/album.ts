import { Injectable } from '@angular/core';

import { BaseProvider } from '../baseProvider';
import { Album } from './../../models/album.model';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Injectable()
export class AlbumProvider extends BaseProvider {

  albunsListRef: FirebaseListObservable<Album[]>;
  private userAuthKey: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afDb: AngularFireDatabase,
    private firebaseApp: FirebaseApp
  ) {
    super();
    this.setAlbunsList();
  }

  private setAlbunsList(): void {
    this.afAuth.authState
      .subscribe((authUser: firebase.User) => {
        if (authUser) {
          this.albunsListRef = this.afDb.list(`/albuns/${authUser.uid}`, {
            query: {
              orderByChild: 'name'
            }
          });
          this.userAuthKey = authUser.uid;
        }
      });
  }

  createAlbum(album: Album): firebase.database.ThenableReference {
    return this.albunsListRef.push(album)
      .catch(this.handleObservableError);
  }

  updateAlbum(album, newObjGame: Album): firebase.Promise<void> {
    return this.albunsListRef.update(album, newObjGame)
      .catch(this.handlePromiseError);
  }

  deleteAlbum(album: Album): firebase.Promise<void> {
    if (album.photo) {
      this.removeAlbumStorage(album.$key);
    }

    return this.albunsListRef.remove(album.$key)
      .then(() => {
        // deletar os jogos do database
        this.afDb.object(`/games/${this.userAuthKey}/${album.$key}`).remove()
          .catch(this.handlePromiseError);
      })
      .catch(this.handlePromiseError);
  }

  uploadImgAlbum(imageUrl: string, albumId: string): firebase.storage.UploadTask {
    return this.firebaseApp.storage()
      .ref()
      .child(`albuns/${this.userAuthKey}/${albumId}`)
      .putString(imageUrl, 'data_url');
  }

  removeAlbumStorage(nameImage: string) {
    this.firebaseApp.storage()
      .ref()
      .child(`albuns/${this.userAuthKey}/${nameImage}`)
      .delete()
      .catch(this.handlePromiseError);
  }

}
