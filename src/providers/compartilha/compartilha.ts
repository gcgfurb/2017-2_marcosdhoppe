import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { BaseProvider } from '../baseProvider';
import { Game } from '../../models/game.model';

@Injectable()
export class CompartilhaProvider extends BaseProvider {

  jogosCompartilhados: FirebaseListObservable<Game[]>;

  constructor(
    private afAuth: AngularFireAuth,
    private afDb: AngularFireDatabase
  ) {
    super();
    this.setJogosList();
  }

  private setJogosList(): void {
    this.afAuth.authState
      .subscribe((authUser: firebase.User) => {
        if (authUser) {
          this.jogosCompartilhados = this.afDb.list(`/compartilha/${authUser.uid}`);
        }
      });
  }

  getAllGamesCompartilhados(): FirebaseListObservable<Game[]> {
    return this.jogosCompartilhados;
  }

  createGameCompartilhado(gameObj: Game, userKey: string): firebase.Promise<void> {
    return this.afDb.object(`compartilha/${userKey}/${gameObj.$key}A`).set(gameObj)
      .catch(this.handlePromiseError);
  }

  deleteGameCompartilhado(gameKey: string): firebase.Promise<void> {
    return this.jogosCompartilhados.remove(gameKey)
      .catch(this.handlePromiseError);
  }

}