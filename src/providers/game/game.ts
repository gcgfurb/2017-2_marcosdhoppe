
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import { BaseProvider } from '../baseProvider';
import { Game } from '../../models/game.model';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Injectable()
export class GameProvider extends BaseProvider {

  jogosRef: FirebaseListObservable<Game[]>;
  userAuthKey: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afDb: AngularFireDatabase,
    private firebaseApp: FirebaseApp
  ) {
    super();
    this.setListDefault();
  }

  private setListDefault(): void {
    this.afAuth.authState
      .subscribe((authUser: firebase.User) => {
        if (authUser) {
          this.jogosRef = this.afDb.list(`games/${authUser.uid}`);
          this.userAuthKey = authUser.uid;
        }
      });
  }

  setRefGame(albumKey: string) {
    this.afAuth.authState
      .subscribe((authUser: firebase.User) => {
        if (authUser) {
          this.jogosRef = this.afDb.list(`games/${authUser.uid}/${albumKey}`);
          this.userAuthKey = authUser.uid;
        }
      });
  }

  getList(): FirebaseListObservable<Game[]> {
    return this.jogosRef;
  }

  createGame(gameObj): firebase.database.ThenableReference {
    return this.jogosRef.push(gameObj)
      .catch(this.handlePromiseError);
  }

  updateGame(gameKey: string, newObjGame: Object): firebase.Promise<void> {
    return this.jogosRef.set(gameKey, newObjGame)
      .catch(this.handlePromiseError);
  }

  deleteGame(gameItem: Game, albumKey: string): firebase.Promise<void> {
    if (gameItem.photo) {
      this.removeGameStorage(gameItem.$key, albumKey);
    }
    return this.jogosRef.remove(gameItem.$key)
      .catch(this.handlePromiseError);
  }

  moveGame(gameObj: Game, albumKey: string): firebase.Promise<void> {
    return this.afDb.object(`games/${this.userAuthKey}/${albumKey}/${gameObj.$key}`).set(gameObj)
      .catch(this.handlePromiseError);
  }

  uploadGameImage(imageUrl: string, albumKey: string, gameKey: string): firebase.storage.UploadTask {
    let caminho = `games/${this.userAuthKey}/${albumKey}/${gameKey}`;
    return this.firebaseApp.storage().ref()
      .child(caminho)
      .putString(imageUrl, 'data_url');
  }

  removeGameStorage(nameImage: string, albumKey: string): firebase.Promise<any> {
    return this.firebaseApp.storage().ref()
      .child(`games/${this.userAuthKey}/${albumKey}/${nameImage}`)
      .delete()
      .catch(this.handlePromiseError);
  }
}
