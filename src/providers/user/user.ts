import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { BaseProvider } from '../baseProvider';
import { User } from './../../models/user.model';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase/app';
import 'firebase/storage';

@Injectable()
export class UserProvider extends BaseProvider {

  listUser: FirebaseListObservable<User[]>;
  currentUser: FirebaseObjectObservable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private afDb: AngularFireDatabase,
    private firebaseApp: FirebaseApp,
  ) {
    super();
    this.listenAuthState();
  }

  // Busca o usuario que esta logado na aplicação, fazendo a incrição para ficar escutando
  // se acontece alguma alteração, e busca a lista de usuarios
  private listenAuthState(): void {
    this.afAuth.authState
      .subscribe((authUser: firebase.User) => {
        if (authUser) {
          this.currentUser = this.afDb.object(`/users/${authUser.uid}`);
          this.setUsers(authUser.uid);
        }
      });
  }

  // retorna todos os usuarios cadastrados ordenando pelo nome, menos o user logado
  private setUsers(uidToExclude: string): void {
    this.listUser = <FirebaseListObservable<User[]>>this.afDb.list(`/users`, {
      query: {
        orderByChild: 'username'
      }
    }).map((users: User[]) => {
      return users.filter((user: User) => user.$key !== uidToExclude);
    });
  }

  // Cria um novo usuario na base de dados
  createUser(user: User, uuid: string): firebase.Promise<void> {
    return this.afDb.object(`/users/${uuid}`).set(user)
      .catch(this.handlePromiseError);
  }

  // Atualiza os dados existentes 
  editUser(user: User): firebase.Promise<void> {
    return this.currentUser.update(user)
      .catch(this.handlePromiseError);
  }

  // Realiza o uploud da foto em dataUrl - base64
  uploadPhotoUser(imgUrl: string, uId: string): firebase.storage.UploadTask {
    return this.firebaseApp.storage().ref()
      .child(`users/${uId}`)
      .putString(imgUrl, 'data_url');
  }

}
