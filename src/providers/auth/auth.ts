import { Injectable } from '@angular/core';

import { BaseProvider } from '../baseProvider';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/first';

@Injectable()
export class AuthProvider extends BaseProvider {

  constructor(private afAuth: AngularFireAuth) {
    super();
  }

  // cria a autenticação para o usuario
  createAuthUser(user: { email: string, password: string }): firebase.Promise<firebase.User> {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .catch(this.handlePromiseError);
  }

  // Loga o usuario e retorna verdadeiro se deu certo 
  signInWithEmail(user: { email: string, password: string }): firebase.Promise<boolean> {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
      .then((authUser: firebase.User) => {
        return authUser != null;
      }).catch(this.handlePromiseError);
  }

  // Desloga o usuario
  signOut(): firebase.Promise<any> {
    return this.afAuth.auth.signOut()
      .catch(this.handlePromiseError);
  }

  // Envia email para resetar o password
  resetPassword(email: string): firebase.Promise<any> {
    return this.afAuth.auth.sendPasswordResetEmail(email)
      .catch(this.handlePromiseError);
  }

  // Verifiica se tem usuario logado - metodo para guarda de rota
  get authenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.afAuth.authState
        .first()
        .subscribe((authUser: firebase.User) => {
          (authUser) ? resolve(true) : reject(false);
        });
    });
  }

}
