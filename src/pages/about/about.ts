import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(private authProvider: AuthProvider) { }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

}
