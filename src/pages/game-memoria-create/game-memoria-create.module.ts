import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameMemoriaCreatePage } from './game-memoria-create';

@NgModule({
  declarations: [
    GameMemoriaCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(GameMemoriaCreatePage),
  ],
})
export class GameMemoriaCreatePageModule {}
