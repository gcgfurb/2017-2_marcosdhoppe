import { Component, ViewChild } from '@angular/core';
import { IonicPage, MenuController, NavController, Slides } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  @ViewChild('slides') slides: Slides;
  showSkip: boolean = true;

  constructor(
    private afAuth: AngularFireAuth,
    private menuCtrl: MenuController,
    private navCtrl: NavController
  ) { }

  // inicia a aplicação, SE tiver alguem logado vai para tabs SENAO login
  startApp() {
    this.afAuth.authState.subscribe((authState: firebase.User) => {
      if (authState) {
        this.navCtrl.setRoot('TabsPage');
      } else {
        this.navCtrl.setRoot('LoginPage');
      }
    });
  }

  // SE for o ultimo slide, esconde o botao skip do topo
  onSlideChangeStart(slider: Slides) {
    this.showSkip = !slider.isEnd();
  }

  // Desabilita o menu lateral quando carregar a página
  ionViewDidEnter() {
    this.menuCtrl.enable(false, 'myMenu');
  }

  // Habilita o menu lateral quando for sair da página
  ionViewWillLeave() {
    this.menuCtrl.enable(true, 'myMenu');
  }

}