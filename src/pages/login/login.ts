import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, IonicPage, Loading, LoadingController, MenuController, NavController, ToastController } from 'ionic-angular';

import { AuthProvider } from './../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  signinForm: FormGroup;

  constructor(
    private alertCtrl: AlertController,
    private authProvider: AuthProvider,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private toastCtrl: ToastController
  ) {

    this.signinForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

  }

  // desabilita o menu lateral quando entra
  ionViewWillEnter(): void  {
    this.menuCtrl.enable(false, 'myMenu');
  }

  // habilita o menu lateral quando sair
  ionViewWillLeave(): void {
    this.menuCtrl.enable(true, 'myMenu');
  }

  onSubmit(): void {
    let loading: Loading = this.showLoading();
    this.authProvider.signInWithEmail(this.signinForm.value)
      .then((isLogged: boolean) => {
        if (isLogged) {
          this.navCtrl.setRoot('TabsPage');
          loading.dismiss();
        }
      }).catch((err: any) => {
        loading.dismiss();
        this.showAlert(err);
      });
  }

  onSingupPage(): void {
    this.navCtrl.push('SingupPage');
  }

  resetPassword(): void {
    const alert = this.alertCtrl.create({
      title: 'Esqueceu sua senha?',
      message: 'Informe seu email cadastrado',
      inputs: [
        {
          type: 'email',
          name: 'email',
          placeholder: 'email@email.com',
          value: this.signinForm.get('email').value
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Enviar',
          handler: data => {
            this.authProvider.resetPassword(data.email)
              .then(() => {
                this.showAlert('A solicitação foi enviada para o seu e-mail.');
              })
              .catch((err: any) => {
                this.showAlert(err);
              });
          }
        }
      ]
    });
    alert.present();
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Por favor aguarde...'
    });
    loading.present();
    return loading;
  }

  private showAlert(text: string): void {
    this.toastCtrl.create({
      message: text,
      position: 'middle',
      showCloseButton: true,
      closeButtonText: "OK"
    }).present();
  }
}
