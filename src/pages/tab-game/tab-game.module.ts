import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabGamePage } from './tab-game';

@NgModule({
  declarations: [
    TabGamePage,
  ],
  imports: [
    IonicPageModule.forChild(TabGamePage),
  ],
})
export class TabGamePageModule {}
