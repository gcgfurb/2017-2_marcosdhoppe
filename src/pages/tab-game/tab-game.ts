import { Game } from './../../models/game.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { GameProvider } from '../../providers/game/game';
import { Subscription } from 'rxjs';
import { FirebaseListObservable } from 'angularfire2/database';
import { GameImportComponent } from '../../components/game-import/game-import';
import { CompartilhaProvider } from '../../providers/compartilha/compartilha';

@IonicPage()
@Component({
  selector: 'page-tab-game',
  templateUrl: 'tab-game.html',
})
export class TabGamePage {

  qtdAlbunsCompart: number = 0;
  list: FirebaseListObservable<any[]>;

  listSubscription: Subscription;

  constructor(
    public authProvider: AuthProvider,
    public gameProvider: GameProvider,
    public compartilhaProvider: CompartilhaProvider,
    private modalCtrl: ModalController,
    public navCtrl: NavController
  ) { }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

  // utilitário para percorrer lista de games dentro dos albums
  objectKeys(obj: any): string[] {
    return Object.keys(obj);
  }

  ionViewDidLoad(): void {
    this.list = this.gameProvider.getList();

    this.listSubscription = this.compartilhaProvider.getAllGamesCompartilhados()
      .subscribe(result => this.qtdAlbunsCompart = result.length)
  }

  ionViewWillUnload(): void {
    this.listSubscription.unsubscribe();
  }

  jogar(game: Game) {
    this.navCtrl.push(this.verificaPag(game, false), { jogoObj: game });
  }

  verificaPag(game: Game, isEditing: boolean): string {
    let page: string;
    switch (game.type) {
      case "Letras":
        page = (isEditing) ? "GameLetrasCreatePage" : "GameLetrasPage";
        break;
      case "Imagens":
        page = (isEditing) ? "GameImagensCreatePage" : "GameImagensPage";
        break;
      case "Memoria":
        page = (isEditing) ? "GameMemoriaCreatePage" : "GameImagensPage";
        break;
      default:
        page = "TutorialPage";
    }
    return page;
  }

  importGame(): void {
    this.modalCtrl.create(GameImportComponent).present();
  }

  filterItems(event: any): void {
    let searchTerm: string = event.target.value;
    console.log(searchTerm);

    /* this.games = this.gameProvider.getAll();
 
     if (searchTerm) {
       this.games = <FirebaseListObservable<Game[]>>this.games
         .map((games: Game[]) => games.filter((game: Game) => (game.name.toLowerCase().indexOf(searchTerm.toLocaleLowerCase()) > -1)));
     }*/
  }




}
