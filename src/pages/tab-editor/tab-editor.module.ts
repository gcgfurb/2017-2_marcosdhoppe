import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabEditorPage } from './tab-editor';

@NgModule({
  declarations: [
    TabEditorPage,
  ],
  imports: [
    IonicPageModule.forChild(TabEditorPage),
  ],
})
export class TabEditorPageModule {}
