import { AlertController, IonicPage, ItemSliding, ModalController, NavController } from 'ionic-angular';
import { Component } from '@angular/core';

import { AlbumCreateComponent } from '../../components/album-create/album-create';
import { Album } from './../../models/album.model';

import { AlbumProvider } from './../../providers/album/album';
import { AuthProvider } from '../../providers/auth/auth';

import { FirebaseListObservable } from 'angularfire2/database';
import { Subscription } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-tab-editor',
  templateUrl: 'tab-editor.html',
})
export class TabEditorPage {

  albumList: FirebaseListObservable<Album[]>;
  qtdAlbuns: number;
  listSubscription: Subscription;

  constructor(
    private authProvider: AuthProvider,
    private albumProvider: AlbumProvider,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private navCtrl: NavController
  ) { }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

  // Busca a lista de albuns (GET)
  ionViewDidLoad(): void {
    this.albumList = this.albumProvider.albunsListRef;
    this.listSubscription = this.albumList.subscribe(result => this.qtdAlbuns = result.length);

  }

  // desisncreve o subscribe para nao dar erro de permição
  ionViewWillUnload() {
    this.listSubscription.unsubscribe();
  }

  // Abre o modal para criar um novo álbum (CREATE) e (UPDATE) do album
  createAndUpdateAlbum(album, slidingItem: ItemSliding): void {
    if (slidingItem) {
      slidingItem.close();
    }
    this.modalCtrl.create(AlbumCreateComponent,
      { albumItem: album }).present();
  }

  openAlbum(album): void {
    this.navCtrl.push('AlbumPage', { albumItem: album });
  }

  delete(album: Album, slidingItem: ItemSliding): void {
    this.alertCtrl.create({
      title: 'Você tem certeza que deseja excluir este álbum?',
      message: 'Esta ação também excluirá todos os jogos deste álbum!',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.albumProvider.deleteAlbum(album);
          }
        },
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            slidingItem.close();
          }
        }
      ]
    }).present();
  }

}
