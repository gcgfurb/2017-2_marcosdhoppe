import { IonicPage, NavController, NavParams, AlertController, ModalController, PopoverController } from 'ionic-angular';
import { Component } from '@angular/core';

import { Album } from '../../models/album.model';
import { Game } from '../../models/game.model';

import { ListAlbumComponent } from './../../components/list-album/list-album';
import { ListUserComponent } from './../../components/list-user/list-user';

import { AuthProvider } from '../../providers/auth/auth';
import { GameProvider } from '../../providers/game/game';

import { FirebaseListObservable } from 'angularfire2/database';
import { Subscription } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-album',
  templateUrl: 'album.html',
})
export class AlbumPage {

  gameList: FirebaseListObservable<Game[]>;
  qtdGames: number;
  album: Album;
  listSubscription: Subscription;

  constructor(
    private authProvider: AuthProvider,
    private alertCtrl: AlertController,
    private gameProvider: GameProvider,
    private modalCtrl: ModalController,
    private navCtrl: NavController,
    private navParams: NavParams,
    private popoverCtrl: PopoverController
  ) {
    this.album = this.navParams.get('albumItem');
    gameProvider.setRefGame(this.album.$key);
  }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

  ionViewDidLoad(): void {
    this.gameList = this.gameProvider.getList();
    this.listSubscription = this.gameList.subscribe(result => this.qtdGames = result.length);
  }

  createNewGame(): void {
    this.navCtrl.push('TemplatesPage', { albumItem: this.album });
  }

  editarGameSelec(game: Game): void {
    this.navCtrl.push(this.verificaPag(game, true), { gameObj: game, albumId: this.album.$key });
  }

  deleteGameList(game: Game): void {
    this.alertCtrl.create({
      message: 'Você deseja excluir este jogo?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.gameProvider.deleteGame(game, this.album.$key);
          }
        },
        {
          text: 'Não',
          role: 'cancel',
        }
      ]
    }).present();
  }

  jogar(game: Game): void {
    this.navCtrl.push(this.verificaPag(game, false), { jogoObj: game });
  }

  compartilhar(game: Game): void {
    this.modalCtrl.create(ListUserComponent, { jogoObj: game }).present();
  }

  moverGame(game: Game): void {
    let popover = this.popoverCtrl.create(ListAlbumComponent, { albumKey: this.album.$key });
    popover.onDidDismiss(albumKey => {
      if (albumKey) {  
        this.gameProvider.moveGame(game, albumKey)
          .then(() => {
            this.gameProvider.deleteGame(game, this.album.$key);
          });
      }
    });
    popover.present({ ev: event });
  }

  verificaPag(game: Game, isEditing: boolean): string {
    let page: string;
    switch (game.type) {
      case "Letras":
        page = (isEditing) ? "GameLetrasCreatePage" : "GameLetrasPage";
        break;
      case "Imagens":
        page = (isEditing) ? "GameImagensCreatePage" : "GameImagensPage";
        break;
      case "Memoria":
        page = (isEditing) ? "GameMemoriaCreatePage" : "GameImagensPage";
        break;
      default:
        page = "TabsPage";
    }
    return page;
  }

  ionViewWillUnload(): void {
    this.listSubscription.unsubscribe();
  }

}