import { Component, ViewChild } from '@angular/core';
import { ActionSheetController, AlertController, IonicPage, Loading, LoadingController, NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { User } from '../../models/user.model';

import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/first';

@IonicPage()
@Component({
  selector: 'page-tab-profile',
  templateUrl: 'tab-profile.html',
})
export class TabProfilePage {

  edita: boolean = false;
  @ViewChild('fileInput') fileInput;
  isUpload: boolean = false;
  currentUser: User = {} as User;
  profileForm: FormGroup;
 

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private authProvider: AuthProvider,
    private camera: Camera,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private userProvider: UserProvider
  ) {

    this.profileForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      username: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      photo: ['']
    });

  }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

  // Inscreve apenas uma vez para pegar o usuario atual, e add ao form
  ionViewDidLoad() {
    this.userProvider.currentUser
      .first()
      .subscribe((currentUser: User) => {
        this.currentUser = currentUser;
        this.profileForm.patchValue(this.currentUser);
      });
  }

  // deslogar o usuario 
  onSingOut(): void {
    this.alertCtrl.create({
      message: 'Você tem certeza que deseja sair?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this.authProvider.signOut()
              .then(() => {
                this.navCtrl.setRoot('LoginPage');
              }).catch((err) => {
                console.error(err);
              });
          }
        },
        {
          text: 'Não',
          role: 'cancel'
        }
      ]
    }).present();
  }

  
  onSubmit(): void {
    if (this.isUpload) {
      this.uploudPhoto();
    } else {
      let usuario: User = this.profileForm.value;
      delete usuario.photo;
      this.userProvider.editUser(usuario);
      this.edita = !this.edita;
    }
  }

  getPicture(): void {
    if (Camera['installed']()) {
      this.onActionSheet();
    } else {
      this.fileInput.nativeElement.click();
    }
  };

  onActionSheet(): void {
    this.actionSheetCtrl.create({
      title: 'Escolha uma opção',
      buttons: [
        {
          text: 'Galeria',
          icon: 'folder-open',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel'
        }
      ]
    }).present();
  }

  // Configurações da camera e abertura dela ou da galeria
  takePicture(canSourceType: number): void {
    let cameraOptions: CameraOptions = {
      correctOrientation: true,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: canSourceType
    };

    this.camera.getPicture(cameraOptions)
      .then((imageData) => {
        this.profileForm.patchValue({ photo: 'data:image/jpeg;base64,' + imageData });
        this.isUpload = true;
      }, (err: Error) => {
        console.log('Erro ao abrir a camera: ', err);
      });
  }

  // Abre o caminho padrao para carregamento de file se nao houver camera
  processWebImage(event): void {
    event.preventDefault();

    let filePhoto: File = event.target.files[0];

    if (filePhoto) {
      let reader = new FileReader();

      reader.onload = (readerEvent) => {
        let imageData = (readerEvent.target as any).result;
        this.profileForm.patchValue({ photo: imageData });
        this.isUpload = true;
      };
      reader.readAsDataURL(filePhoto);
    }
  }

  // Realiza o upload da foto para o firebase 
  uploudPhoto(): void {
    let loading: Loading = this.showLoading();
    let uploadTask = this.userProvider.uploadPhotoUser(this.profileForm.get('photo').value, this.currentUser.$key);

    uploadTask.on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
      let uploadProgress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
      console.log(uploadProgress);

    }, (error: Error) => {
      console.log("erro ao carregar imagem");
    });

    uploadTask.then((UploadTaskSnapshot: firebase.storage.UploadTaskSnapshot) => {
      // console.log("upload feito com sucesso");
      this.profileForm.patchValue({ photo: uploadTask.snapshot.downloadURL });
      this.userProvider.editUser(this.profileForm.value);
      loading.dismiss();
      this.edita = !this.edita;
    });
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Por favor aguarde o upload da imagem.'
    });
    loading.present();
    return loading;
  }
}
