import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { Game } from './../../models/game.model';

import { AuthProvider } from '../../providers/auth/auth';
import { GameProvider } from '../../providers/game/game';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-game-letras-create',
  templateUrl: 'game-letras-create.html',
})
export class GameLetrasCreatePage {

  @ViewChild('fileInput') fileInput;
  gameForm: FormGroup;
  gameItem: Game;
  isImgUpload: boolean = false;
  albumKey: string;

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private authProvider: AuthProvider,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private gameProvider: GameProvider,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    public navParams: NavParams
  ) {

    this.gameItem = navParams.get('gameObj') || {} as Game;
    this.albumKey = navParams.get('albumId');

    gameProvider.setRefGame(this.albumKey);

    this.gameForm = this.formBuilder.group({
      name: [this.gameItem.name || '', [Validators.required, Validators.minLength(3)]],
      pergunta: [this.gameItem.pergunta || '', [Validators.required]],
      resposta: [this.gameItem.resposta || '', [Validators.required]],
      dificuldade: [this.gameItem.dificuldade || '9', [Validators.required]],
      photo: [this.gameItem.photo || ''],
      tempo: [this.gameItem.tempo || '00:00:00', [Validators.required]],
      type: ['Letras'],
    });
  }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

  onSubmit(): void {
    let gameCreate = this.gameForm.value;
   
    if (gameCreate.photo == "") {
      delete gameCreate.photo;
    }

    if (!this.gameItem.$key) {
      this.gameProvider.createGame(gameCreate).then(resultGame => {
        if (this.isImgUpload) {
          this.uploadImage(this.gameForm.get('photo').value, resultGame.key);
        } else {
          this.navCtrl.remove(2, 2);
        }
      });

    } else {
      if (this.isImgUpload) {
        this.uploadImage(this.gameForm.get('photo').value, this.gameItem.$key);
      } else {
        this.gameProvider.updateGame(this.gameItem.$key, gameCreate);
        this.navCtrl.pop();
      }
    }
  }

  getPicture(): void {
    if (Camera['installed']()) {
      this.onActionSheet();
    } else {
      this.fileInput.nativeElement.click();
    }
  };

  onActionSheet(): void {
    this.actionSheetCtrl.create({
      title: 'Escolha uma opção',
      buttons: [
        {
          text: 'Galeria',
          icon: 'folder-open',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel'
        }
      ]
    }).present();
  }

  // Configurações da camera e abertura dela ou da galeria
  takePicture(canSourceType: number): void {
    let cameraOptions: CameraOptions = {
      correctOrientation: true,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: canSourceType
    };

    this.camera.getPicture(cameraOptions)
      .then((imageData) => {
        this.gameForm.patchValue({ photo: 'data:image/jpeg;base64,' + imageData });
        this.isImgUpload = true;
      }, (err: Error) => {
        console.log('Erro ao abrir a camera: ', err);
      });
  }

  processWebImage(event): void {
    event.preventDefault();
    let filePhoto: File = event.target.files[0];

    if (filePhoto) {
      let reader = new FileReader();
      reader.onload = (readerEvent) => {
        let imageData = (readerEvent.target as any).result;
        this.gameForm.patchValue({ photo: imageData });
        this.isImgUpload = true;
      };
      reader.readAsDataURL(filePhoto);
    }
  }

  uploadImage(imgDataUrl: string, gameKey: string): void {
    let loading: Loading = this.showLoading();

    let uploadTask = this.gameProvider.uploadGameImage(imgDataUrl, this.albumKey, gameKey);

    uploadTask.on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {
      let uploadProgress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
      console.log("Progress: ", uploadProgress);

    }, (error: Error) => {
      console.log("Erro ao carregar a imagem")
    });

    uploadTask.then((UploadTaskSnapshot: firebase.storage.UploadTaskSnapshot) => {
      console.log("upload feito com sucesso");
      this.gameForm.patchValue({ photo: uploadTask.snapshot.downloadURL });
      this.gameProvider.updateGame(gameKey, this.gameForm.value);
      loading.dismiss();

      if (this.gameItem.$key) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.remove(2, 2);
      }

    });
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Por favor aguarde o upload da imagem.'
    });
    loading.present();
    return loading;
  }
}
