import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameLetrasCreatePage } from './game-letras-create';

@NgModule({
  declarations: [
    GameLetrasCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(GameLetrasCreatePage),
  ],
})
export class GameLetrasCreatePageModule {}
