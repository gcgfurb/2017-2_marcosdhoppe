import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Album } from '../../models/album.model';
import { GameProvider } from '../../providers/game/game';
import { Game } from '../../models/game.model';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-game-imagens-create',
  templateUrl: 'game-imagens-create.html',
})
export class GameImagensCreatePage {

  @ViewChild('filesInput') fileInput;
  gameForm: FormGroup;
  imgListE: string[] = [];
  imgListC: string[] = [];
  gameItem: Game;
  album: Album;

  constructor(
    public authProvider: AuthProvider,
    public formBuilder: FormBuilder,
    public gameProvider: GameProvider,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.album = navParams.get('albumItem');

    this.gameForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      resposta: ['', [Validators.required, Validators.maxLength(12)]],
      dica: ['', [Validators.required, Validators.minLength(2)]],
      type: ['Imagens'],
      tempo: ['00:00:00'],
      photos: this.formBuilder.array([this.createItem()])
    });

  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      photo: ''
    });
  }

  onSubmit() {
    let game: Game = {} as Game;

    this.gameProvider.createGame(game).then(() => {
      console.log("jogo salvo no banco criado com sucesso");
      this.navCtrl.remove(2, 2);
    });
  }

  getFiles() {
    this.fileInput.nativeElement.click();
  }

  onChange(event): void {
    event.preventDefault();

    let files: File[] = event.target.files;

    for (var i = 0; i < files.length; i++) {
      let file = files[i];

      if (!file.type.match('image.*'))
        continue;

      let reader = new FileReader();

      reader.onload = (readerEvent) => {
        let imageData = (readerEvent.target as any).result;
        this.imgListE.push(imageData);
      };
      reader.readAsDataURL(file);
    }
    files = undefined;
  }

  deleteImg(i: number, arrayImg: boolean): void {
    if (arrayImg) {
      this.imgListC.splice(i, 1);
    } else {
      this.imgListE.splice(i, 1);
    }
  }

  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

 /* private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Por favor aguarde o upload da imagem.'
    });
    loading.present();
    return loading;
  }
*/
}

