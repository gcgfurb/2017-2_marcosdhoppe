import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameImagensCreatePage } from './game-imagens-create';

@NgModule({
  declarations: [
    GameImagensCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(GameImagensCreatePage),
  ],
})
export class GameImagensCreatePageModule {}
