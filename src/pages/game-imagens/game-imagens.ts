import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams } from 'ionic-angular';

export interface Item {
  image: string;
  imgCorrect: boolean;
  clicked: boolean;
}

@IonicPage()
@Component({
  selector: 'page-game-imagens',
  templateUrl: 'game-imagens.html',
})
export class GameImagensPage {

  jogo: any = {} as any;
  continuaJogo: boolean = true;
  itens: Item[] = [];

  constructor(
    public navCtrl: NavController, 
    public alertCtrl: AlertController,
    public navParams: NavParams
  ) {
    this.iniciaAtributoJogo();
    this.inicializaVetorImg();
  }


  verificaImagens(): void {
    for (var i = 0; i < this.itens.length; i++) {
      if (this.itens[i].imgCorrect === this.itens[i].clicked) {
        this.continuaJogo = false;
      } else {
        this.continuaJogo = true;
        break;
      }
    }

    if (!this.continuaJogo) {
      this.presentAlert("Parabéns!!! você acertou todas as imagens.");
    } else {
      this.presentAlert("Ops... imagens selecionas incorretas ou faltando! Tente novamente.");
    }
  }


  presentAlert(mensagem: string): void {
    const alert = this.alertCtrl.create({
      message: mensagem,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }

  shuffle(array: any[]): any {
    return array.sort(() => { return 0.5 - Math.random() });
  }

  inicializaVetorImg(): void {
    for (var i = 0; i < this.jogo.imagensCertas.length; i++) {
      this.itens.push({ image: this.jogo.imagensCertas[i], imgCorrect: true, clicked: false })
    }
    for (var j = 0; j < this.jogo.imagensErradas.length; j++) {
      this.itens.push({ image: this.jogo.imagensErradas[j], imgCorrect: false, clicked: false })
    }

    this.shuffle(this.itens);
    this.shuffle(this.itens);
  }

  iniciaAtributoJogo(): void {
    this.jogo.name = "jogo das imagens";
    this.jogo.imagensCertas = ["assets/img/user1.png", "assets/img/user1.png", "assets/img/user1.png", "assets/img/user1.png"];
    this.jogo.imagensErradas = ["assets/img/user2.png", "assets/img/user3.png", "assets/img/user2.png", "assets/img/user4.png"];
    this.jogo.tempo = "00:00:00";
    this.jogo.pergunta = "Quais animais abaixo são ursos?";
  }
}