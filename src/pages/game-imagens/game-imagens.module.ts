import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameImagensPage } from './game-imagens';

@NgModule({
  declarations: [
    GameImagensPage,
  ],
  imports: [
    IonicPageModule.forChild(GameImagensPage),
  ],
})
export class GameImagensPageModule {}
