import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, Loading, LoadingController, MenuController, NavController, ToastController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-singup',
  templateUrl: 'singup.html',
})
export class SingupPage {

  singupForm: FormGroup;

  constructor(
    private authProvider: AuthProvider,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private userProvider: UserProvider,
    private toastCtrl: ToastController
  ) {

    this.singupForm = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
      username: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(6)]]

    });
  }

  // desabilita o menu lateral quando entrar na página
  ionViewWillEnter(): void {
    this.menuCtrl.enable(false, 'myMenu');
  }

  // Habilita o menu lateral quando for sair da página
  ionViewWillLeave(): void {
    this.menuCtrl.enable(true, 'myMenu');
  }

  
  onSubmit(): void {
    let loading: Loading = this.showLoading();
    let usuario = this.singupForm.value;

    this.authProvider.createAuthUser({ email: usuario.email, password: usuario.password })
      .then((authUser: firebase.User) => {

        delete usuario.password;
        let uuid: string = authUser.uid;

        this.userProvider.createUser(usuario, uuid)
          .then(() => {
            console.log('Usuario cadastrado com sucesso!');
            this.navCtrl.setRoot('TabsPage');
            loading.dismiss();

          }).catch((err: any) => {
            loading.dismiss();
            this.showAlert(err);
          });
      }).catch((err: any) => {
        loading.dismiss();
        this.showAlert(err);
      });
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Por favor aguarde...'
    });
    loading.present();
    return loading;
  }

  private showAlert(text: string): void {
    this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'middle'
    }).present();
  }
}