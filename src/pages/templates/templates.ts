import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Album } from '../../models/album.model';

@IonicPage()
@Component({
  selector: 'page-templates',
  templateUrl: 'templates.html',
})
export class TemplatesPage {

  album: Album;

  templates: any[] = [
    { img: 'assets/img/templates/letters.png', title: 'Letras', pageName: 'GameLetrasCreatePage' },
    { img: 'assets/img/templates/postcards.png', title: 'Imagens', pageName: 'GameImagensCreatePage' },
    { img: 'assets/img/templates/memoria.png', title: 'Memória', pageName: 'GameMemoriaCreatePage' },
    { img: 'assets/img/templates/puzzle.png', title: 'Quebra-Cabeça', pageName: 'GameMemoriaCreatePage' }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.album = navParams.get('albumItem');
  }

  openLayout(template) {
    this.navCtrl.push(template.pageName, { albumId: this.album.$key });
  }
}
