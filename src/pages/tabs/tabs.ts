import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root: string = 'TabEditorPage';
  tab2Root: string = 'TabGamePage';
  tab3Root: string = 'TabProfilePage';
  myIndex: number;

  constructor(private authProvider: AuthProvider, private navParams: NavParams) {
    this.myIndex = this.navParams.data.tabIndex || 1;
  }

  // Guarda de Rota (SE usuario logado true; SENAO false)
  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }
}