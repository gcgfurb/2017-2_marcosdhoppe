import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameLetrasPage } from './game-letras';

@NgModule({
  declarations: [
    GameLetrasPage,
  ],
  imports: [
    IonicPageModule.forChild(GameLetrasPage),
  ],
})
export class GameLetrasPageModule {}
