import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, FabContainer } from 'ionic-angular';

import { Game } from '../../models/game.model';

@IonicPage()
@Component({
  selector: 'page-game-letras',
  templateUrl: 'game-letras.html',
})
export class GameLetrasPage {

  jogo: Game;
  dica: string = "";
  qtdErro: number = 0;
  letra: string = "";
  letraDigitadas: string = "";
  vetorLetras: string[] = [];

  constructor(
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.jogo = navParams.get('jogoObj') || {} as Game;
    this.iniciaVetorVazio();
  }

  iniciaVetorVazio(): void {
    this.jogo.resposta = this.jogo.resposta.replace(" ", "-");

    for (var i = 0; i < this.jogo.resposta.length; i++) {
      if (this.jogo.resposta[i].indexOf("-")) {
        this.vetorLetras.push("_");
      } else {
        this.vetorLetras.push("-");
      }
    }
  }

  verificaLetra(): void {
    if (this.letra != "") {
      this.letra = this.letra[0].toUpperCase();

      if (this.jogo.resposta.toUpperCase().indexOf(this.letra) == -1) {                   // verifica se existe a letra de entrada na palavra
        this.qtdErro++;

        if (this.qtdErro == 1) {                                                          // tratamento do , das letras
          this.letraDigitadas = this.letra;
        } else {
          this.letraDigitadas = this.letraDigitadas + ", " + this.letra;
        }
        this.letra = "";

        if ("" + this.qtdErro == this.jogo.dificuldade) {                                 // verifica fim do jogo pela quantidade de erros
          this.presentAlert("Game Over", "Tente novamente!");
        }

      } else {
        for (var i = 0; i < this.jogo.resposta.length; i++) {                             // verifica todas as letras da palavra em maiuscula 
          if (this.jogo.resposta.toUpperCase()[i] == this.letra) {
            this.vetorLetras[i] = this.jogo.resposta[i];

            if (this.vetorLetras.indexOf("_") == -1) {                                    // verifica se ja encontrou todas as letras
              this.presentAlert("Parabéns!!!", "Você ganhou o jogo");
            }
          }

        }
        this.letra = "";
      }
    }

  }


  abrirDica(tipoDica: string, fab: FabContainer): void {
    this.dica = tipoDica;
    fab.close();
  }

  presentAlert(titulo: string, mensagem: string): void {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensagem,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }
}