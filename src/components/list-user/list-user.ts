import { Component } from '@angular/core';
import { NavParams, ViewController, ToastController } from 'ionic-angular';

import { FirebaseListObservable } from 'angularfire2/database';

import { CompartilhaProvider } from '../../providers/compartilha/compartilha';
import { UserProvider } from '../../providers/user/user';

import { Game } from '../../models/game.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'list-user',
  templateUrl: 'list-user.html'
})
export class ListUserComponent {

  jogo: Game;
  users: FirebaseListObservable<User[]>;

  constructor(
    private compartilhaProvider: CompartilhaProvider,
    private navParams: NavParams,
    private userProvider: UserProvider,
    private viewCtrl: ViewController,
    private toastCtrl: ToastController
  ) {
    this.jogo = this.navParams.get('jogoObj');
  }

  // pega lista de usuarios
  ionViewDidLoad(): void {
    this.users = this.userProvider.listUser;
  }

  // fecha o janela modal 
  close(): void  {
    this.viewCtrl.dismiss();
  }

  // Compartilha o jogo no estado que ele se encontra
  compartilhaGameonUser(user: User): void {
    this.compartilhaProvider.createGameCompartilhado(this.jogo, user.$key)
      .then(() => {
        this.viewCtrl.dismiss();
        this.showAlert("Jogo Compartilhado com sucesso!");
      })
      .catch((err: any) => {
        this.showAlert(err);
      });
  }

  // apresenta a mensagem
  private showAlert(text: string): void {
    this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: 'middle'
    }).present();
  }
}