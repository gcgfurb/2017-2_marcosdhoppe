import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

import { FirebaseListObservable } from 'angularfire2/database';

import { AlbumProvider } from '../../providers/album/album';

import { Game } from '../../models/game.model';
import { Album } from '../../models/album.model';

@Component({
  selector: 'list-album',
  templateUrl: 'list-album.html'
})
export class ListAlbumComponent {

  listAlbum: FirebaseListObservable<Album[]>;
  game: Game;
  albumKeyAtual: string;

  constructor(
    private albumProvider: AlbumProvider,
    private viewCtrl: ViewController,
    private navParams: NavParams
  ) {
    this.albumKeyAtual = this.navParams.get('albumKey');
  }

  ionViewDidLoad(): void {
    if (!this.albumKeyAtual) {
      this.listAlbum = this.albumProvider.albunsListRef;
    } else {
      this.listAlbum = <FirebaseListObservable<Album[]>>this.albumProvider.albunsListRef
        .map((albuns: Album[]) => {
          return albuns.filter((album: Album) => album.$key !== this.albumKeyAtual);
        });
    }
  }

  acceptGame(albumKey: string): void {
    this.viewCtrl.dismiss(albumKey);
  }

}