import { Component } from '@angular/core';
import { PopoverController, ViewController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { CompartilhaProvider } from '../../providers/compartilha/compartilha';

import { Game } from '../../models/game.model';

import { FirebaseListObservable } from 'angularfire2/database';
import { ListAlbumComponent } from '../list-album/list-album';
import { GameProvider } from '../../providers/game/game';
import { Subscription } from 'rxjs';

@Component({
  selector: 'game-import',
  templateUrl: 'game-import.html'
})
export class GameImportComponent {

  listGames: FirebaseListObservable<Game[]>;
  listSubscription: Subscription;

  constructor(
    private authProvider: AuthProvider,
    private compartilhaProvider: CompartilhaProvider,
    private gameProvider: GameProvider,
    private popoverCtrl: PopoverController,
    private viewCtrl: ViewController
  ) { }

  ionViewDidLoad(): void {
    this.listGames = this.compartilhaProvider.getAllGamesCompartilhados();
    this.listSubscription = this.listGames.subscribe(
      result => {
        if (result.length == 0) {
          this.viewCtrl.dismiss();
        }
      });
  }

  aceitarJogo(game: Game): void {
    let popover = this.popoverCtrl.create(ListAlbumComponent);
    popover.onDidDismiss(albumKey => {
      if (albumKey) {
        this.gameProvider.moveGame(game, albumKey)
          .then(() => {
            this.compartilhaProvider.deleteGameCompartilhado(game.$key);
          });
      }
    });
    popover.present({ ev: event });
  }

  recusarJogo(game: Game): void {
    this.compartilhaProvider.deleteGameCompartilhado(game.$key);
  }

  close(): void {
    this.viewCtrl.dismiss();
  }

  ionViewCanEnter(): Promise<boolean> {
    return this.authProvider.authenticated;
  }

  ionViewCanLeave(): boolean {
    this.listSubscription.unsubscribe();
    return true;
  }

}
