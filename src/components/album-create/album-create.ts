
import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Loading, LoadingController, NavParams, ViewController, ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { AlbumProvider } from '../../providers/album/album';
import { Album } from './../../models/album.model';
import * as firebase from 'firebase/app';

@Component({
  selector: 'album-create',
  templateUrl: 'album-create.html'
})
export class AlbumCreateComponent {

  @ViewChild('fileInput') fileInput;
  album: Album;
  albumForm: FormGroup;
  isImgUpload: boolean = false;
  isEdit = false;

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private albumProvider: AlbumProvider,
    private camera: Camera,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private navParams: NavParams,
    private viewCtrl: ViewController
  ) {

    this.album = this.navParams.get("albumItem");

    if (this.album) {
      this.isEdit = true;
    } else {
      this.album = {} as Album;
    }

    this.albumForm = this.formBuilder.group({
      name: [this.album.name || '', [Validators.required, Validators.minLength(3)]],
      description: [this.album.description || '', [Validators.required, Validators.maxLength(120)]],
      photo: [this.album.photo || '']
    });

  }

  salvar(): void {
    let albumItem: Album = this.albumForm.value;
    delete albumItem.photo;

    if (this.isEdit) {
      if (this.isImgUpload) {
        this.uploadImage(this.albumForm.get('photo').value, this.album.$key);
      } else {
        this.albumProvider.updateAlbum(this.album.$key, albumItem);
        this.viewCtrl.dismiss();
      }
    } else {
      this.albumProvider.createAlbum(albumItem).then(result => {
        if (this.isImgUpload) {
          this.uploadImage(this.albumForm.get('photo').value, result.key);
        } else {
          this.viewCtrl.dismiss();
        }
      });
    }
  }

  getPicture(): void {
    if (Camera['installed']()) {
      this.onActionSheet();
    } else {
      this.fileInput.nativeElement.click();
    }
  };

  onActionSheet(): void {
    this.actionSheetCtrl.create({
      title: 'Escolha uma opção',
      buttons: [
        {
          text: 'Galeria',
          icon: 'folder-open',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel'
        }
      ]
    }).present();
  }

  takePicture(canSourceType: number): void {
    let cameraOptions: CameraOptions = {
      correctOrientation: true,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: canSourceType
    };

    this.camera.getPicture(cameraOptions)
      .then((imageData) => {
        this.albumForm.patchValue({ photo: 'data:image/jpeg;base64,' + imageData });
        this.isImgUpload = true;
      }, (err: Error) => {
        console.log('Erro ao abrir a camera: ', err);
      });
  }

  processWebImage(event): void {
    event.preventDefault();

    let filePhoto: File = event.target.files[0];

    if (filePhoto) {
      let reader = new FileReader();

      reader.onload = (readerEvent) => {
        let imageData = (readerEvent.target as any).result;
        this.albumForm.patchValue({ photo: imageData });
        this.isImgUpload = true;
      };
      reader.readAsDataURL(filePhoto);
    }
  }

  uploadImage(imagem64: string, albumID: string): void {
    let loading: Loading = this.showLoading();
    let uploadTask = this.albumProvider.uploadImgAlbum(imagem64, albumID);

    uploadTask.on('state_changed', (snapshot: firebase.storage.UploadTaskSnapshot) => {

    }, (error: Error) => {
      console.log("erro carregar imagem")
    });

    uploadTask.then((UploadTaskSnapshot: firebase.storage.UploadTaskSnapshot) => {
      this.albumForm.patchValue({ photo: uploadTask.snapshot.downloadURL });
      this.albumProvider.updateAlbum(albumID, this.albumForm.value);
      loading.dismiss();
      this.viewCtrl.dismiss();
    });
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Por favor aguarde o upload da imagem.'
    });
    loading.present();
    return loading;
  }

  close(): void {
    this.viewCtrl.dismiss();
  }

}
