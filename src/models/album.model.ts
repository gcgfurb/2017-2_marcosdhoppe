export interface Album {
    $key?: string;
    name: string;
    description: string;
    photo?: string;
}