export interface Game {
    $key?: string;
    name: string;
    pergunta: string;
    resposta: string;
    type: string;
    dificuldade: string;
    tempo: string;
    photo?: string;
}